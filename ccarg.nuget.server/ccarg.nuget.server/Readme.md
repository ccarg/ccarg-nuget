﻿ push example:
 nuget.exe push WebActivatorEx.2.2.0.nupkg apiKey -Source http://localhost:50022/nuget

 https://github.com/jchadwick/NuGet.Packer

https://docs.microsoft.com/en-us/nuget/hosting-packages/nuget-server

Configuring the Packages folder
With NuGet.Server 1.5 and later, you can more specifically configure the package folder using the appSetting/packagesPath value in web.config:

XML

Copy
<appSettings>
    <!-- Set the value here to specify your custom packages folder. -->
    <add key="packagesPath" value="C:\MyPackages" />
</appSettings>
packagesPath can be an absolute or virtual path.

When packagesPath is omitted or left blank, the packages folder is the default ~/Packages.

Adding packages to the feed externally
Once a NuGet.Server site is running, you can add packages using nuget push provided that you set an API key value in web.config.

After installing the NuGet.Server package, web.config contains an empty appSetting/apiKey value:

XML

Copy
<appSettings>
    <add key="apiKey" value="" />
</appSettings>
When apiKey is omitted or blank, pushing packages to the feed is disabled.

To enable this capability, set the apiKey to a value (ideally a strong password) and add a key called appSettings/requireApiKey with the value of true:

XML

Copy
<appSettings>
        <!-- Sets whether an API Key is required to push/delete packages -->
    <add key="requireApiKey" value="true" />

    <!-- Set a shared password (for all users) to push/delete packages -->
    <add key="apiKey" value="" />
</appSettings>
If your server is already secured or you do not otherwise require an API key (for example, when using a private server on a local team network), you can set requireApiKey to false. All users with access to the server can then push packages.

Removing packages from the feed
With NuGet.Server, the nuget delete command removes a package from the repository provided that you include the API key with the comment.

If you want to change the behavior to delist the package instead (leaving it available for package restore), change the enableDelisting key in web.config to true.

<PropertyGroup>
 <PublishNuGetPackage>true</PublishNuGetPackage>
 <NuGetApiKey>apiKey</NuGetApiKey>
 <NuGetPublishSource>http://localhost:50022/nuget</NuGetPublishSource>
</PropertyGroup>